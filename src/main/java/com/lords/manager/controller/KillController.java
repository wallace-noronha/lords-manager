package com.lords.manager.controller;

import com.lords.manager.model.PlayerDTO;
import com.lords.manager.model.PlayerModel;
import com.lords.manager.service.ConvertXlsService;
import com.lords.manager.service.PlayerService;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.ParseException;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@RestController()
@RequestMapping("/kills")
public class KillController {

    private PlayerService playerService;
    private ConvertXlsService convertXlsService;

    @Autowired
    public KillController(PlayerService playerService, ConvertXlsService convertXlsService){
        this.playerService = playerService;
        this.convertXlsService = convertXlsService;
    }

    @GetMapping
    public ResponseEntity<List<PlayerModel>> getAll(
            @ApiParam(name = "startDate", value = "Data de ínicio para busca", required = true)
            @RequestHeader(name = "startDate") String startDate,
            @ApiParam(name = "endDate", value = "Data de fim para a busca", required = true)
            @RequestHeader(name = "endDate") String endDate
    ) throws ParseException {
        return ResponseEntity.ok(playerService.getAllPlayers(startDate,endDate));
    }

    @PostMapping("/uploadFile")
    public ResponseEntity<String> uploadFile(@RequestParam("file") MultipartFile file) throws IOException, ParseException {

        Path filepath = Files.createTempFile("tmp", file.getOriginalFilename());

        try (OutputStream os = Files.newOutputStream(filepath)) {
            os.write(file.getBytes());
        }
        String date = file.getOriginalFilename().replaceAll(".*-","").replaceAll("\\.[^.]*$","").replaceAll("\\.","/");

        playerService.savePlayers(convertXlsService.convert(filepath, date));
        Files.deleteIfExists(filepath);
        return ResponseEntity.ok("Arquivo inserido com sucesso!");
    }
}
