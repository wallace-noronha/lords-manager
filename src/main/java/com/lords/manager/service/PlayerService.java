package com.lords.manager.service;

import com.lords.manager.model.PlayerModel;
import com.lords.manager.model.PlayerModelComparable;
import com.lords.manager.repository.KillRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigInteger;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.TimeZone;
import java.util.function.Supplier;

@Service
public class PlayerService {

    private KillRepository killRepository;

    @Autowired
    PlayerService(KillRepository killRepository){
        this.killRepository = killRepository;
    }


    public List<PlayerModel> getAllPlayers(String startDate, String endDate) throws ParseException {

        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
        formatter.setTimeZone(TimeZone.getTimeZone("UTC"));
        if (formatter.parse(startDate).getTime() == formatter.parse(endDate).getTime()){
            return killRepository.findPlayer(formatter.parse(startDate).getTime());
        } else {
            return this.getManyData(formatter.parse(startDate).getTime(), formatter.parse(endDate).getTime());
        }

    }

    public void savePlayers(List<PlayerModel> playerModels){
        killRepository.saveAll(playerModels);
    }

    private List<PlayerModel> getManyData(Long startDate, Long endDate){

        LinkedHashMap<Integer, PlayerModelComparable> finalPlayerModel = new LinkedHashMap<>();
        List<PlayerModel> players = killRepository.findPlayers(startDate, endDate);
        for (PlayerModel player: players){
            if (finalPlayerModel.containsKey(player.getId())){
                if (player.getData() > finalPlayerModel.get(player.getId()).getData()){
                    finalPlayerModel.get(player.getId()).setKills(player.getKills());
                    finalPlayerModel.get(player.getId()).setPoder(player.getPoder());
                    finalPlayerModel.get(player.getId()).setNome(player.getNome());
                    finalPlayerModel.get(player.getId()).setPoderDestruido(player.getPoderDestruido());
                    finalPlayerModel.get(player.getId()).setRank(player.getRank());
                    finalPlayerModel.get(player.getId()).setTropasPerdidas(player.getTropasPerdidas());
                }
                finalPlayerModel.get(player.getId()).getSumKills().addLast(player.getKills());
                finalPlayerModel.get(player.getId()).getSumPoderPerdido().addLast(player.getPoder());
                finalPlayerModel.get(player.getId()).getSumTropasPerdidas().addLast(player.getTropasPerdidas());
                finalPlayerModel.get(player.getId()).getSumPoderDestruido().addLast(player.getPoderDestruido());
            } else {
                PlayerModelComparable playerModelComparable = new PlayerModelComparable(player.getId(),player.getNome(),player.getRank(),player.getPoder(),player.getKills(),player.getPoderDestruido(),player.getTropasPerdidas(),player.getData());
                playerModelComparable.setSumKills(new LinkedList<>(Arrays.asList(player.getKills())));
                playerModelComparable.setSumPoderDestruido(new LinkedList<>(Arrays.asList(player.getPoderDestruido())));
                playerModelComparable.setSumPoderPerdido(new LinkedList<>(Arrays.asList(player.getPoder())));
                playerModelComparable.setSumTropasPerdidas(new LinkedList<>(Arrays.asList(player.getTropasPerdidas())));
                finalPlayerModel.put(player.getId(),playerModelComparable);
            }

        }

        return compare(finalPlayerModel);
    }

    private List<PlayerModel> compare(LinkedHashMap<Integer, PlayerModelComparable> players){
        List<PlayerModel> playerModels = new ArrayList<>();
        for (Integer player: players.keySet()){
            PlayerModel playerModel = new PlayerModel(
                    players.get(player).getId(),
                    players.get(player).getNome(),
                    players.get(player).getRank(),
                    players.get(player).getPoder(),
                    players.get(player).getKills(),
                    players.get(player).getPoderDestruido(),
                    players.get(player).getTropasPerdidas(),
                    players.get(player).getData()
            );
            playerModel.setSumKills(players.get(player).getSumKills().getLast().subtract(players.get(player).getSumKills().getFirst()));
            playerModel.setSumPoderDestruido(players.get(player).getSumPoderDestruido().getLast().subtract(players.get(player).getSumPoderDestruido().getFirst()));
            playerModel.setSumPoderPerdido(players.get(player).getSumPoderPerdido().getLast().subtract(players.get(player).getSumPoderPerdido().getFirst()));
            playerModel.setSumTropasPerdidas(players.get(player).getSumTropasPerdidas().getLast().subtract(players.get(player).getSumTropasPerdidas().getFirst()));

            playerModels.add(playerModel);
        }

        return playerModels;

    }


}
