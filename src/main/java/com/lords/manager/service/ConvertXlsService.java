package com.lords.manager.service;


import com.lords.manager.model.PlayerDTO;
import com.lords.manager.model.PlayerModel;
import com.poiji.bind.Poiji;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;

import java.io.File;
import java.math.BigInteger;
import java.nio.file.Path;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

@Service
public class ConvertXlsService {

    public List<PlayerModel> convert(Path filepath, String data) throws ParseException {
        List<PlayerModel> playerModelList = new ArrayList<>();
        SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");
        formatter.setTimeZone(TimeZone.getTimeZone("UTC"));
        Date date = formatter.parse(data);

        File file = new File(filepath.toString());
        List<PlayerDTO> players = Poiji.fromExcel(file, PlayerDTO.class);
        for (PlayerDTO player : players){
            playerModelList.add(
                    new PlayerModel(
                            player.getId(),
                            player.getNome(),
                            player.getRank(),
                            new BigInteger(player.getPoder().replace(",","")),
                            new BigInteger(player.getKills().replace(",","")),
                            new BigInteger(player.getPoderDestruido().replace(",","")),
                            new BigInteger(player.getTropasPerdidas().replace(",","")),
                            date.getTime()
                    )
            );
        }

        return playerModelList;
    }

}
