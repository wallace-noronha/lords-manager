package com.lords.manager.repository;

import com.lords.manager.model.PlayerModel;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.List;


public interface KillRepository extends CrudRepository<PlayerModel,Object> {

    @Query("{ 'data' : { '$gte': ?0, '$lte': ?1 }}")
    List<PlayerModel> findPlayers(Long from, Long to);

    @Query("{ 'data' : ?0 }")
    List<PlayerModel> findPlayer(Long from);

    @Override
    <S extends PlayerModel> Iterable<S> saveAll(Iterable<S> entities);
}
