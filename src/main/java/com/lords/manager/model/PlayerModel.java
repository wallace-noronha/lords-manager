package com.lords.manager.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import java.math.BigInteger;

@Document(collection = "status")
public class PlayerModel {

    @Id
    private ObjectId _id;
    private Integer id;
    private String nome;
    private String rank;
    private BigInteger poder;
    private BigInteger kills;
    @Field("poder_destruido")
    @JsonProperty("poder_destruido")
    private BigInteger poderDestruido;
    @JsonProperty("tropas_perdidas")
    private BigInteger tropasPerdidas;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty("soma_kills")
    private BigInteger sumKills;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty("soma_poder_perdido")
    private BigInteger sumPoderPerdido;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty("soma_tropas_perdidas")
    private BigInteger sumTropasPerdidas;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty("soma_poder_destruido")
    private BigInteger sumPoderDestruido;
    private Long data;

    public PlayerModel(){}

    public PlayerModel(Integer id, String nome, String rank, BigInteger poder,BigInteger kills,BigInteger poderDestruido,BigInteger tropasPerdidas, Long data){
        this.id = id;
        this.nome = nome;
        this.rank = rank;
        this.poder = poder;
        this.kills = kills;
        this.poderDestruido = poderDestruido;
        this.tropasPerdidas = tropasPerdidas;
        this.data = data;
    }

    public ObjectId get_id() {
        return _id;
    }

    public void set_id(ObjectId _id) {
        this._id = _id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getRank() {
        return rank;
    }

    public void setRank(String rank) {
        this.rank = rank;
    }

    public BigInteger getPoder() {
        return poder;
    }

    public void setPoder(BigInteger poder) {
        this.poder = poder;
    }

    public BigInteger getKills() {
        return kills;
    }

    public void setKills(BigInteger kills) {
        this.kills = kills;
    }

    public BigInteger getPoderDestruido() {
        return poderDestruido;
    }

    public void setPoderDestruido(BigInteger poderDestruido) {
        this.poderDestruido = poderDestruido;
    }

    public BigInteger getTropasPerdidas() {
        return tropasPerdidas;
    }

    public void setTropasPerdidas(BigInteger tropasPerdidas) {
        this.tropasPerdidas = tropasPerdidas;
    }

    public BigInteger getSumPoderPerdido() {
        return sumPoderPerdido;
    }

    public void setSumPoderPerdido(BigInteger sumPoderPerdido) {
        this.sumPoderPerdido = sumPoderPerdido;
    }

    public BigInteger getSumTropasPerdidas() {
        return sumTropasPerdidas;
    }

    public void setSumTropasPerdidas(BigInteger sumTropasPerdidas) {
        this.sumTropasPerdidas = sumTropasPerdidas;
    }

    public BigInteger getSumPoderDestruido() {
        return sumPoderDestruido;
    }

    public void setSumPoderDestruido(BigInteger sumPoderDestruido) {
        this.sumPoderDestruido = sumPoderDestruido;
    }

    public Long getData() {
        return data;
    }

    public void setData(Long data) {
        this.data = data;
    }

    public BigInteger getSumKills() {
        return sumKills;
    }

    public void setSumKills(BigInteger sumKills) {
        this.sumKills = sumKills;
    }
}
