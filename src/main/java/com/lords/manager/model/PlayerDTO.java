package com.lords.manager.model;

import com.poiji.annotation.ExcelCellName;

import java.util.Date;

public class PlayerDTO {


    @ExcelCellName("User ID")
    private Integer id;
    @ExcelCellName("Name")
    private String nome;
    @ExcelCellName("Rank")
    private String rank;
    @ExcelCellName("Might")
    private String poder;
    @ExcelCellName("Kills")
    private String kills;
    @ExcelCellName("Enemies Destroyed (Might)")
    private String poderDestruido;
    @ExcelCellName("Troops Lost")
    private String tropasPerdidas;
    private Date data;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getRank() {
        return rank;
    }

    public void setRank(String rank) {
        this.rank = rank;
    }

    public String getPoder() {
        return poder;
    }

    public void setPoder(String poder) {
        this.poder = poder;
    }

    public String getKills() {
        return kills;
    }

    public void setKills(String kills) {
        this.kills = kills;
    }

    public String getPoderDestruido() {
        return poderDestruido;
    }

    public void setPoderDestruido(String poderDestruido) {
        this.poderDestruido = poderDestruido;
    }

    public String getTropasPerdidas() {
        return tropasPerdidas;
    }

    public void setTropasPerdidas(String tropasPerdidas) {
        this.tropasPerdidas = tropasPerdidas;
    }

    public Date getData() {
        return data;
    }

    public void setData(Date data) {
        this.data = data;
    }
}
